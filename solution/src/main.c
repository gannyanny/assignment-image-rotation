#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include <stdlib.h>


int main( int argc, char** argv ) {
    if (argc != 3)
    {
        printf("Неверное количество аргументов\n");
        return 0;
    }

    struct image img = {0};
    
    struct image rotated_img = {0};
    
    FILE *file = fopen(argv[1], "rb");
    
    FILE *new_file = fopen(argv[2], "wb");

    switch (from_bmp(file, &img))
    {
    case READ_NULL:
        printf("Ошибка прочтения\n");
        return 1;
        break;
    case READ_INVALID_SIGNATURE:
        printf("Ошибка в чтении сигнатуры\n");
        return 1;
        break;
    case READ_INVALID_BITS:
        printf("Ошибка в чтении разрядов\n");
        return 1;
        break;
    case READ_INVALID_HEADER:
        printf("Ошибка в чтении заголовка\n");
        return 1;
        break;
    case SEEK_ERROR:
        printf("Ошибка поиска\n");
        return 1;
        break;
    case READ_OK:
        printf("Картинка прочиталась\n");
        break;
    }

    rotated_img = rotate(img);

    switch (to_bmp(new_file, &rotated_img))
    {
    case WRITE_ERROR:
        printf("Ошибка\n");
        return 1;
        break;
    case WRITE_HEADER_ERROR:
        printf("Ошибка записи заголовка\n");
        return 1;
        break;
    case WRITE_PIXEL_ERROR:
        printf("Ошибка записи пикселя\n");
        return 1;
        break;
    case WRITE_PADDING_ERROR:
        printf("Ошибка заполнения\n");
        return 1;
        break;
    case WRITE_OK:
        printf("Картинка записана\n");
        break;
    }

    fclose(file);
    fclose(new_file);
    free(img.data);
    free(rotated_img.data);
    return 0;
}
